<?php

namespace app\klase;

use Yii;

class FiltersStats extends \yii\base\Model
{

    public $start_year;
    public $end_year;
    
/**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_year', 'end_year'], 'date', 'format' => 'php:Y-m-d']
        ];
    }

    public function attributeLabels()
    {
        return [
            'start_year' => 'Start date',
            'end_year' => 'End date',
        ];
    }
}
