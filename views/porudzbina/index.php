<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PorudzbinaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Porudzbina';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="message">
<?php
if (Yii::$app->session->getFlash('success')) {
    echo Yii::$app->session->getFlash('success');
} else {
    echo Yii::$app->session->getFlash('error');
}
?>
</div>

<div class="porudzbina-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?php if (Yii::$app->user->isGuest) {
    Yii::$app->response->redirect(['/user/security/login']);
}
    ?>
        <?= Html::a('Napravi Porudzbinu', ['create'], ['class' => 'btn btn-primary']) ?>
        <?php if ($nameOfUser == 'Supervisor') {?>
            <?= Html::a('Unesi Nedelju', ['nedelja/create'], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Odredi cenu obroka', ['odredi-cenu/create'], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Posalji Email', ['send'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Izvestaj dugovanja', ['reports/debts'], ['class' => 'btn btn-warning']) ?>
        <?php } ?>
        <?= Html::a('Statistika', ['reports/rucak-meseca'], ['class' => 'btn btn-info']) ?>
    </p>
    <form action = "get">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pageSummaryRowOptions' => ['style'=>'text-align: right;'],
        //'showPageSummary' => true,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['label' => 'Ime',
            'attribute' => 'ime',
            'value' => 'osoba.ime',
            ],
            ['label' => 'Prezime',
            'attribute' => 'prezime',
            'value' => 'osoba.prezime',
            ],
            'glavnoJelo.ime_jela',
            'prilog.ime_priloga',
            'salata.ime_salate',
            'hleb.ime_hleba',
            'created_on',
            [
                'attribute' => 'cena',
                'pageSummary' => true,
                'format' => ['currency',''],
                'footer' => $total,
            
            ],

            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{update} {delete}'],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function ($dataProvider, $key, $index, $column) {
                    return ['value' => $dataProvider->id_porudzbina];
                },
                'cssClass' => 'checkbox',
              
            ],
        ],
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton('Delete', ['class' => 'btn btn-primary','id' => 'btn']) ?>
    </div>
   </form>

<?php
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'osoba.ime',
    'glavnoJelo.ime_jela',
    'prilog.ime_priloga',
    'salata.ime_salate',
    [
        'attribute' => 'cena',
        'pageSummary' => true,
        'format' => ['currency',''],
        'footer' => $total,
    
    ],
    ['class' => 'yii\grid\ActionColumn'],
];

// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'filename' => 'porudzbine'
]);


?>
</div>