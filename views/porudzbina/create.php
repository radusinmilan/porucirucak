<?php

use yii\helpers\Html;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Porudzbina */

$this->title = 'Porudzbina';
$this->params['breadcrumbs'][] = ['label' => 'Porudzbina', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="porudzbina-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rez' => $rez,
        'nameOfUser' => $nameOfUser,
        'osoba' => $osoba,
        'jela' => $jela,
    ]) ?>

</div>
