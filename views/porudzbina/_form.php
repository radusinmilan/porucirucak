<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Osoba;
use app\models\GlavnoJelo;
use app\models\Prilog;
use app\models\Salata;
use app\models\Nedelja;
use app\models\Hleb;

/* @var $this yii\web\View */
/* @var $model app\models\Porudzbina */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="porudzbina-form">

    <?php $form = ActiveForm::begin(); ?>
  
    <p>Napravite Vasu porudzbinu: <strong><h4><?= Html::encode($nameOfUser) ?></h4></strong></p><br>
    <?php
    if ($nameOfUser == 'Supervisor') {
        echo $form->field($model, 'id_osoba')->dropDownList(
            ArrayHelper::map($osoba, 'id_osoba', 'ime'),
            ['prompt'=>'--Odaberite osobu--']
        );
    }?>

    <?= $form->field($model, 'id_glavno_jelo')->dropDownList(
        ArrayHelper::map($jela, 'id_glavno_jelo', 'ime_jela'),
        ['prompt'=>'--Odaberite jelo--']
    ) ?>

    <?= $form->field($model, 'id_prilog')->dropDownList(
        ArrayHelper::map(Prilog::find()->asArray()->all(), 'id_prilog', 'ime_priloga'),
        ['prompt'=>'--Odaberite prilog--']
    ) ?>

    <?= $form->field($model, 'id_salata')->dropDownList(
        ArrayHelper::map(Salata::find()->asArray()->all(), 'id_salata', 'ime_salate'),
        ['prompt'=>'--Odaberite salatu--']
    ) ?>

    <?= $form->field($model, 'id_hleb')->dropDownList(
        ArrayHelper::map(Hleb::find()->asArray()->all(), 'id_hleb', 'ime_hleba'),
        ['prompt'=>'--Odaberite hleb--']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
