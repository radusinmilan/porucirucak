<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Porudzbina */

$this->title = "Vasa porudzbina";
$this->params['breadcrumbs'][] = ['label' => 'Porudzbina', 'url' => ['porudzbina/index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="porudzbina-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_porudzbina',
            'id_osoba',
            'id_glavno_jelo',
            'id_prilog',
            'id_salata',
            'id_hleb',
            'created_on',
        ],
    ]) ?>

</div>
