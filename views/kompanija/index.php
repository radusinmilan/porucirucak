<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KompanijaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kompanija';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kompanija-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kompanija', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'naziv',
            'email:email',
            'kontakt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
