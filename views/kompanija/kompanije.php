<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Kompanija;

/* @var $this yii\web\View */
/* @var $model app\models\Kompanija */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kompanija-form">

    <?php $form = ActiveForm::begin(['action'=>'index.php?r=porudzbina/send'],['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'naziv')
    ->dropDownList(ArrayHelper::map(Kompanija::find()->asArray()->all(), 'naziv', 'naziv'),
    ['prompt'=>' -- Kompanije --']) ?>

    <?= $form->field($model,'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>