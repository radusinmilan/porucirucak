<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

?>

<div>
<h3><strong><?= Html::encode($message)?></strong></h3>
</div>
<hr>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        //'pageSummaryRowOptions' => ['style'=>'text-align: right;'],
        //'showPageSummary' => true,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['label' => 'Ime',
            'attribute' => 'ime',
            'value' => 'ime',
            ],
            ['label' => 'Prezime',
            'attribute' => 'prezime',
            'value' => 'prezime',
            ],
          
            [
                'attribute' => 'dugovanje',
                'pageSummary' => true,
                //'format' =>['currency',''],
                //'footer' => $total,
            
            ],

            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{update} {delete}'],
          
        ],
    ]); ?>
    <div class="form-group">
        <?php $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'ime',
        'prezime',
        [
        'attribute' => 'dugovanje',
        'pageSummary' => true,
        'format' => ['currency',''],
        //'footer' => $total,
    
        ],
        ['class' => 'yii\grid\ActionColumn'],
];

// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'filename' => 'dugovanje'. '' .date('yyyy-mm-dd'),
]); ?>
<hr>
    </div>