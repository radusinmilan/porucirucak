<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<div class="filters-form">
    <?php $form = ActiveForm::begin(); ?>
     
    <?= $form->field($model, 'start_year')
        ->dropDownList(
            $items,
            ['prompt'=>'From...']
        ); ?>

<?= $form->field($model, 'end_year')
        ->dropDownList(
            $items,
            ['prompt'=>'To...']
        ); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>