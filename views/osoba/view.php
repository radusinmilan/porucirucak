<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Osoba */

$this->title = $model->id_osoba;
$this->params['breadcrumbs'][] = ['label' => 'Osobas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="osoba-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_osoba',
            'ime',
            'prezime',
        ],
    ]) ?>

</div>
