<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Prilog */

$this->title = $model->id_prilog;
$this->params['breadcrumbs'][] = ['label' => 'Prilogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="prilog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_prilog',
            'ime_priloga',
        ],
    ]) ?>

</div>
