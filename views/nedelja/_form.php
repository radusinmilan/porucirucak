<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Nedelja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nedelja-form">
<?php $nedelja = array('1'=>'1','2'=>'2','3'=>'3','4'=>'4');?>
   <p>Trenutni broj nedelje je: <strong> <?=$trenutnaNedelja?> </strong></p>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'broj_nedelje')->dropDownList(
    $nedelja,
    ['prompt'=>'']
)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
