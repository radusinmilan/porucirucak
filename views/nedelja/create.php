<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nedelja */

$this->title = 'Unesi Nedelju';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nedelja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'trenutnaNedelja' => $trenutnaNedelja,
    ]) ?>

</div>
