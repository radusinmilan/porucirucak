<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nedelja */

$this->title = 'Update Nedelja: ' . $model->id_nedelja;
$this->params['breadcrumbs'][] = ['label' => 'Nedeljas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nedelja, 'url' => ['view', 'id' => $model->id_nedelja]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nedelja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
