<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Hleb */

$this->title = $model->id_hleb;
$this->params['breadcrumbs'][] = ['label' => 'Hlebs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hleb-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_hleb',
            'ime_hleba',
        ],
    ]) ?>

</div>
