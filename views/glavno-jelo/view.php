<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GlavnoJelo */

$this->title = $model->id_glavno_jelo;
$this->params['breadcrumbs'][] = ['label' => 'Glavno Jelos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="glavno-jelo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_glavno_jelo',
            'ime_jela',
            'nedelja',
        ],
    ]) ?>

</div>
