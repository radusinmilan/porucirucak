<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dan */

$this->title = 'Create Dan';
$this->params['breadcrumbs'][] = ['label' => 'Dans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
