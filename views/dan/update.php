<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dan */

$this->title = 'Update Dan: ' . $model->id_dan;
$this->params['breadcrumbs'][] = ['label' => 'Dans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_dan, 'url' => ['view', 'id' => $model->id_dan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
