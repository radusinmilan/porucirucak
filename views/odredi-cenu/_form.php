<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OdrediCenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="odredi-cenu-form">
   <p>Trenutna cena je: <strong> <?=$trenutnaCena?> Rsd </strong></p>
    <?php $form = ActiveForm::begin(); ?>
     
    <?= $form->field($model, 'trenutna_cena', ['inputOptions' => [
    'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
