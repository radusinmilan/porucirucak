<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OdrediCenu */

$this->title = 'Odredi Cenu';
//$this->params['breadcrumbs'][] = ['label' => 'Odredi Cenus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="odredi-cenu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'trenutnaCena' => $trenutnaCena
    ]) ?>

</div>
