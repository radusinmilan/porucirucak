<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OdrediCenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Odredi Cenu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="odredi-cenu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Odredi Cenu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_cena',
            'trenutna_cena',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
