<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OdrediCenu */

$this->title = 'Update Odredi Cenu: ' . $model->id_cena;
$this->params['breadcrumbs'][] = ['label' => 'Odredi Cenus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_cena, 'url' => ['view', 'id' => $model->id_cena]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="odredi-cenu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
