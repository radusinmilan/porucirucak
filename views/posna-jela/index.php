<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosnaJelaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posna Jelas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posna-jela-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Posna Jela', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_posno_jelo',
            'ime_posnog_jela',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
