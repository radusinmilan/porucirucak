<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosnaJela */

$this->title = 'Update Posna Jela: ' . $model->id_posno_jelo;
$this->params['breadcrumbs'][] = ['label' => 'Posna Jelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_posno_jelo, 'url' => ['view', 'id' => $model->id_posno_jelo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posna-jela-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
