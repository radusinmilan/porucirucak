<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosnaJela */

$this->title = 'Create Posna Jela';
$this->params['breadcrumbs'][] = ['label' => 'Posna Jelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posna-jela-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
