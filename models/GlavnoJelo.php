<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "glavno_jelo".
 *
 * @property int $id_glavno_jelo
 * @property string $ime_jela
 * @property int $nedelja
 *
 * @property Porudzbina[] $porudzbinas
 */
class GlavnoJelo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'glavno_jelo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nedelja','dan'], 'integer'],
            [['ime_jela'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_glavno_jelo' => 'Id Glavno Jelo',
            'ime_jela' => 'Glavno Jelo',
            'nedelja' => 'Nedelja',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorudzbina()
    {
        return $this->hasMany(Porudzbina::className(), ['id_glavno_jelo' => 'id_glavno_jelo']);
    }

    public static function getMeal($jelo) {

        $sql = "SELECT ime_jela from glavno_jelo where nedelja = $jelo";
        $rez = Yii::$app->db->createCommand($sql)->queryAll();

        return $rez;
    }
}
