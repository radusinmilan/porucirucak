<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kompanija".
 *
 * @property int $id
 * @property string $naziv
 * @property string $email
 * @property int $kontakt
 */
class Kompanija extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kompanija';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
            [['kontakt'], 'integer'],
            [['naziv', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'naziv' => 'Naziv',
            'email' => 'Email',
            'kontakt' => 'Kontakt',
            'file' => 'File',
        ];
    }

    public function kompanija($naziv) {
        $sql = "SELECT email FROM kompanija WHERE naziv = '$naziv'";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }
}
