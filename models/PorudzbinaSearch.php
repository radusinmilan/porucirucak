<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Porudzbina;
use app\models\Osoba;

/**
 * PorudzbinaSearch represents the model behind the search form of `app\models\Porudzbina`.
 */
class PorudzbinaSearch extends Porudzbina
{
    /**
     * {@inheritdoc}
     */


    public $ime;
    public $prezime;
    public function rules()
    {
        return [
            [['id_porudzbina', 'id_osoba', 'id_glavno_jelo', 'id_prilog', 'id_salata', 'id_hleb'], 'integer'],
            [['created_on', 'ime', 'prezime'], 'safe'],
            [['created_on', 'ime', 'prezime'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $identityy = false)
    {
  
        if ($identityy != null) {
            if ($identityy->username == 'Supervisor' || $identityy->username == null) {
                $query = Porudzbina::find()
                ->joinWith('osoba')->orderBy([
                'created_on'=>SORT_DESC,
                'ime'=>SORT_ASC,
                ]);
            } else {
                $personName = $identityy->username;
                (int)$personId = $identityy->id;
                $query = Porudzbina::find()
                ->joinWith('osoba')
                ->andWhere('osoba.ime ='."'$personName'")
                ->orderBy([
                    'created_on'=>SORT_DESC,
                    'ime'=>SORT_ASC,
                    ]);
            }
        } else {
            $query = Porudzbina::find()
                ->joinWith('osoba')->orderBy([
                'created_on'=>SORT_DESC,
                'ime'=>SORT_ASC,
                ]);
        }
        
        // add conditions that should always apply here
        //echo $query->createCommand()->getRawSql();die;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_porudzbina' => $this->id_porudzbina,
            'id_osoba' => $this->id_osoba,
            'id_glavno_jelo' => $this->id_glavno_jelo,
            'id_prilog' => $this->id_prilog,
            'id_salata' => $this->id_salata,
            'id_hleb' => $this->id_hleb,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'osoba.ime', $this->ime])
        ->andFilterWhere(['like', 'osoba.prezime', $this->prezime]);

        return $dataProvider;
    }
}
