<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posna_jela".
 *
 * @property int $id_posno_jelo
 * @property string $ime_posnog_jela
 */
class PosnaJela extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posna_jela';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ime_posnog_jela'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_posno_jelo' => 'Id Posno Jelo',
            'ime_posnog_jela' => 'Ime Posnog Jela',
        ];
    }
}
