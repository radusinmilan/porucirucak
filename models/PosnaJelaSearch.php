<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PosnaJela;

/**
 * PosnaJelaSearch represents the model behind the search form of `app\models\PosnaJela`.
 */
class PosnaJelaSearch extends PosnaJela
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_posno_jelo'], 'integer'],
            [['ime_posnog_jela'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PosnaJela::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_posno_jelo' => $this->id_posno_jelo,
        ]);

        $query->andFilterWhere(['like', 'ime_posnog_jela', $this->ime_posnog_jela]);

        return $dataProvider;
    }
}
