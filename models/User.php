<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        
        '21' => [
            'id' => '21',
            'username' => 'supervisor',
            'password' => 'supervisor44',
            'authKey' => 'test21key',
            'accessToken' => '21-token',
        ],
        
        '1' => [
            'id' => '1',
            'username' => 'milan',
            'password' => 'milan44',
            'authKey' => 'test1key',
            'accessToken' => '1-token',
        ],
        '3' => [
            'id' => '3',
            'username' => 'Boban',
            'password' => 'boban44',
            'authKey' => 'test3key',
            'accessToken' => '3-token',
        ],
        '2' => [
            'id' => '2',
            'username' => 'Nemanja',
            'password' => 'nemanja44',
            'authKey' => 'test2key',
            'accessToken' => '2-token',
        ],
        '4' => [
            'id' => '4',
            'username' => 'Bogdan',
            'password' => 'bogdan44',
            'authKey' => 'test4key',
            'accessToken' => '4-token',
        ],
        '5' => [
            'id' => '5',
            'username' => 'Vladimir',
            'password' => 'vladimir44',
            'authKey' => 'test5key',
            'accessToken' => '5-token',
        ],
        '18' => [
            'id' => '18',
            'username' => 'Marko',
            'password' => 'marko44',
            'authKey' => 'test18key',
            'accessToken' => '18-token',
        ],
        '10' => [
            'id' => '10',
            'username' => 'Filip',
            'password' => 'filip44',
            'authKey' => 'test10key',
            'accessToken' => '10-token',
        ],
        '13' => [
            'id' => '13',
            'username' => 'ZoranGrcic',
            'password' => 'zorangrcic44',
            'authKey' => 'test13key',
            'accessToken' => '13-token',
        ],
        '9' => [
            'id' => '9',
            'username' => 'BojanaR',
            'password' => 'bojanar44',
            'authKey' => 'test9key',
            'accessToken' => '9-token',
        ],
        '15' => [
            'id' => '15',
            'username' => 'BojanaM',
            'password' => 'bojanam44',
            'authKey' => 'test15key',
            'accessToken' => '15-token',
        ],
        '7' => [
            'id' => '7',
            'username' => 'Mirela',
            'password' => 'mirela44',
            'authKey' => 'test7key',
            'accessToken' => '7-token',
        ],
        '8' => [
            'id' => '8',
            'username' => 'Nina',
            'password' => 'nina44',
            'authKey' => 'test8key',
            'accessToken' => '8-token',
        ],
        '6' => [
            'id' => '6',
            'username' => 'Damjan',
            'password' => 'damjan44',
            'authKey' => 'test6key',
            'accessToken' => '6-token',
        ],
        '17' => [
            'id' => '17',
            'username' => 'Dragan',
            'password' => 'dragan44',
            'authKey' => 'test17key',
            'accessToken' => '17-token',
        ],
        '16' => [
            'id' => '16',
            'username' => 'Boris',
            'password' => 'boris44',
            'authKey' => 'test16key',
            'accessToken' => '16-token',
        ],
        '19' => [
            'id' => '19',
            'username' => 'ZoranGojkovic',
            'password' => 'zorangojkovic44',
            'authKey' => 'test19key',
            'accessToken' => '19-token',
        ],
        '11' => [
            'id' => '11',
            'username' => 'Natasa',
            'password' => 'natasa44',
            'authKey' => 'test11key',
            'accessToken' => '11-token',
        ],
        '12' => [
            'id' => '12',
            'username' => 'Nikola',
            'password' => 'nikola44',
            'authKey' => 'test12key',
            'accessToken' => '12-token',
        ],
    ];


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
