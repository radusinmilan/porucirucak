<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nedelja".
 *
 * @property int $id_nedelja
 * @property int $broj_nedelje
 */
class Nedelja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nedelja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['broj_nedelje'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nedelja' => 'Id Nedelja',
            'broj_nedelje' => 'Broj Nedelje',
        ];
    }


    public function updateWeek($brojNedelje)
    {
        $sql ="UPDATE nedelja SET broj_nedelje = $brojNedelje WHERE broj_nedelje IS NOT NULL;";
        Yii::$app->db->createCommand($sql)->execute();
    }

    public function returnWeek()
    {
        $sql = "SELECT broj_nedelje FROM nedelja WHERE broj_nedelje IS NOT NULL";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }
}
