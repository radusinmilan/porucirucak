<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dan".
 *
 * @property int $id_dan
 * @property string $dan
 *
 * @property GlavnoJelo[] $glavnoJelos
 */
class Dan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_dan' => 'Id Dan',
            'dan' => 'Dan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getGlavnoJelos()
    {
        return $this->hasMany(GlavnoJelo::className(), ['dan' => 'id_dan']);
    }

    public function getDay()
    {
        $dan = array('1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7');
        return $dan;
    }
}
