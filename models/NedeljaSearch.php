<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Nedelja;

/**
 * NedeljaSearch represents the model behind the search form of `app\models\Nedelja`.
 */
class NedeljaSearch extends Nedelja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nedelja', 'broj_nedelje'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nedelja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nedelja' => $this->id_nedelja,
            'broj_nedelje' => $this->broj_nedelje,
        ]);

        return $dataProvider;
    }
}
