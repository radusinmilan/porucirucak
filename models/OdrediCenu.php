<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "odredi_cenu".
 *
 * @property int $id_cena
 * @property string $trenutna_cena
 */
class OdrediCenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'odredi_cenu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trenutna_cena'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cena' => 'Id Cena',
            'trenutna_cena' => 'Unesite Novu Cenu',
        ];
    }

    public function updatePrice($cena)
    {
        $sql ="UPDATE `odredi_cenu` SET `trenutna_cena` = $cena WHERE `trenutna_cena` IS NOT NULL;";
        Yii::$app->db->createCommand($sql)->execute();
    }

    public static function getPrice()
    {
        $sql = "SELECT trenutna_cena FROM odredi_cenu WHERE trenutna_cena IS NOT NULL";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }
}
