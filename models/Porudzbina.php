<?php

namespace app\models;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Yii;

/**
 * This is the model class for table "porudzbina".
 *
 * @property int $id_porudzbina
 * @property int $id_osoba
 * @property int $id_glavno_jelo
 * @property int $id_prilog
 * @property int $id_salata
 * @property int $id_hleb
 * @property string $created_on
 *
 * @property Hleb $hleb
 * @property GlavnoJelo $glavnoJelo
 * @property Osoba $osoba
 * @property Prilog $prilog
 * @property Salata $salata
 */
class Porudzbina extends \yii\db\ActiveRecord
{
    public $nedelja;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'porudzbina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_osoba', 'id_glavno_jelo', 'id_prilog', 'id_salata', 'id_hleb'], 'safe'],
            [['id_osoba', 'id_glavno_jelo', 'id_prilog', 'id_salata', 'id_hleb', 'nedelja'], 'integer'],
            [['created_on'], 'safe'],
            [['id_hleb'], 'exist', 'skipOnError' => true, 'targetClass' => Hleb::className(), 'targetAttribute' => ['id_hleb' => 'id_hleb']],
            [['id_glavno_jelo'], 'exist', 'skipOnError' => true, 'targetClass' => GlavnoJelo::className(), 'targetAttribute' => ['id_glavno_jelo' => 'id_glavno_jelo']],
            [['id_osoba'], 'exist', 'skipOnError' => true, 'targetClass' => Osoba::className(), 'targetAttribute' => ['id_osoba' => 'id_osoba']],
            [['id_prilog'], 'exist', 'skipOnError' => true, 'targetClass' => Prilog::className(), 'targetAttribute' => ['id_prilog' => 'id_prilog']],
            [['id_salata'], 'exist', 'skipOnError' => true, 'targetClass' => Salata::className(), 'targetAttribute' => ['id_salata' => 'id_salata']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nedelja' => 'Nedelja',
            'id_porudzbina' => 'Id Porudzbina',
            'id_osoba' => 'Osoba',
            'id_glavno_jelo' => 'Glavno Jelo',
            'id_prilog' => 'Prilog',
            'id_salata' => ' Salata',
            'id_hleb' => ' Hleb',
            'created_on' => 'Za Datum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHleb()
    {
        return $this->hasOne(Hleb::className(), ['id_hleb' => 'id_hleb']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGlavnoJelo()
    {
        return $this->hasOne(GlavnoJelo::className(), ['id_glavno_jelo' => 'id_glavno_jelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsoba()
    {
        return $this->hasOne(Osoba::className(), ['id_osoba' => 'id_osoba']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrilog()
    {
        return $this->hasOne(Prilog::className(), ['id_prilog' => 'id_prilog']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalata()
    {
        return $this->hasOne(Salata::className(), ['id_salata' => 'id_salata']);
    }

    // vadjenje jela za zadatu nedelju
    public function jeloPoNedelji($nedelja)
    {
        return  $query = GlavnoJelo::find()->where(['nedelja'=>$nedelja])->all();
    }

    // Podaci za export u excel
    public static function sqlDataExcel()
    {
        $todayDate = date('Y-m-d');
        $datum = date('Y-m-d', strtotime($todayDate . "+1 days"));
        return Porudzbina::find()
               ->select(
                   [
                   'osoba.ime AS Ime',
                   'osoba.prezime AS Prezime',
                   'glavno_jelo.ime_jela AS Jelo',
                   'salata.ime_salate AS salata',
                   'hleb.ime_hleba AS Hleb'
                   ]
               )
               ->leftJoin('osoba', 'porudzbina.id_osoba = osoba.id_osoba')
               ->leftJoin('glavno_jelo', 'porudzbina.id_glavno_jelo = glavno_jelo.id_glavno_jelo')
               ->leftJoin('prilog', 'porudzbina.id_prilog = prilog.id_prilog')
               ->leftJoin('salata', 'porudzbina.id_salata = salata.id_salata')
               ->leftJoin('hleb', 'porudzbina.id_hleb = hleb.id_hleb')
               ->andwhere(['created_on'=>$datum])
               ->asArray()
               ->all();
    }

    // Export u excel
    public static function exportExcel($data, $spreadsheet)
    {
        @unlink('Avangarda.xlsx');
        $row = 1; // 1-based index
        $col = 1;
            
        $worksheet = $spreadsheet->getActiveSheet()->fromArray(array_keys($data[0]));
        $spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        foreach ($data as $row => $columns) {
            foreach ($columns as $column => $dataa) {
                $worksheet->setCellValueByColumnAndRow($col, $row+2, $dataa);
                $col++;
            }
            $col=1;
            $row++;
        }

        $spreadsheet->getDefaultStyle()
        ->getBorders()
        ->getTop()
        ->setBorderStyle(Border::BORDER_THIN);

        header("Content-Type: application/xlsx");
        header("Pragma: no-cache");
        header("Expires: 0");

        $writer = new Xlsx($spreadsheet);
        $writer->save('Avangarda.xlsx');
    }

    // slanje email-a
    public static function sendMail($setTo, $setSubject, $setBody, $atachFile)
    {
        $send = Yii::$app->mailer->compose()
        ->setTo($setTo)
        ->setSubject($setSubject)
        ->setTextBody($setBody)
        ->attach($atachFile)
        ->send();

        if ($send) {
            return true;
        }
        return false;
    }

    // Total cena za index footer
    public function getTotalPrice($dataProvider)
    {
        $data = $dataProvider->getModels();
        $total = 0;
        foreach ($data as $d) {
            $total+=$d->cena;
        }

        return number_format($total, 2);
    }

    public static function getLunchOfTheMonth($model)
    {
        $where = "";

        if (!empty($model->start_year) && !empty($model->end_year)) {
            $where = "AND (porudzbina.created_on BETWEEN '{$model->start_year}' AND '{$model->end_year}')";
        }
        
        $sql = "SELECT
                gj.ime_jela, count(gj.ime_jela) as Jelo
            FROM
                porudzbina
            LEFT JOIN glavno_jelo gj
            ON gj.id_glavno_jelo = porudzbina.id_glavno_jelo
            {$where}
            GROUP BY gj.ime_jela";
        
        /*

            return Porudzbina::find()->select(['ime_jela, count(ime_jela) as Jelo'])
            ->leftJoin(['`glavno_jelo` ON `glavno_jelo`.`id_glavno_jelo` = `porudzbina`.`id_glavno_jelo`'])
            //->andWhere($where)
            ->limit(10)->groupBy(['ime_jela'])->asArray()->all();

        */

        return  Yii::$app->db->createCommand($sql)->queryAll();
    }

    public static function getOrderDate()
    {
        return Porudzbina::find()
        ->select('created_on')
        ->orderBy(['created_on' => SORT_ASC])
        ->all();
    }

    public static function getDebtReport()
    {
        $sql = "select osoba.ime,osoba.prezime,sum(porudzbina.cena) as dugovanje from porudzbina 
        left join osoba on osoba.id_osoba = porudzbina.id_osoba
        group by osoba.ime";
        //return Yii::$app->db->createCommand($sql)->queryScalar();
        
        return Porudzbina::find()
        ->select('osoba.ime,osoba.prezime,sum(porudzbina.cena) as dugovanje')
        ->leftJoin('`osoba` on `osoba`.`id_osoba` = `porudzbina`.`id_osoba`')
        ->groupBy('osoba.ime');
    }
}
