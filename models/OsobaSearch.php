<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Osoba;

/**
 * OsobaSearch represents the model behind the search form of `app\models\Osoba`.
 */
class OsobaSearch extends Osoba
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_osoba'], 'integer'],
            [['ime', 'prezime'], 'safe'],
            [['ime', 'prezime'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Osoba::find()->orderBy([
            'ime'=>SORT_ASC
          ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_osoba' => $this->id_osoba,
        ]);

        $query->andFilterWhere(['like', 'ime', $this->ime])
            ->andFilterWhere(['like', 'prezime', $this->prezime]);

        return $dataProvider;
    }
}
