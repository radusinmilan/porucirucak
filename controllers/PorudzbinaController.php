<?php

namespace app\controllers;

use Yii;
use app\models\Porudzbina;
use app\models\PorudzbinaSearch;
//use app\models\User;
use dektrium\user\models\User;
use app\models\Nedelja;
use app\models\PosnaJela;
use app\models\Osoba;
use app\models\GlavnoJelo;
use app\models\OdrediCenu;
use app\models\Kompanija;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\base\Exception;

/**
 * PorudzbinaController implements the CRUD actions for Porudzbina model.
 */
class PorudzbinaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Porudzbina models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isGet) {
            $values = Yii::$app->request->get();
            // delete if selected checkBox
            if ($values && array_key_exists('selection', $values)) {
                $data = $values["selection"];
                foreach ($data as $id) {
                    $this->findModel($id)->delete();
                }
                return $this->redirect('index');
            }
        }

        $debt = Porudzbina::getDebtReport();
        
        $nameOfUser = null;
        //find user
        $identityy = User::findIdentity(Yii::$app->user->id);
        //$personId = (int) $identity->id;
        //$imeOsobe = Osoba::getOsobaLikeUser($identityy->username);
        
        
        if ($identityy != null) {
            $nameOfUser =$identityy->username;
        }
        
        $searchModel = new PorudzbinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $identityy);
        //Total za footer
        $total = $searchModel->getTotalPrice($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nameOfUser' =>  $nameOfUser,
            'total' => $total,
        ]);
    }

    /**
     * Displays a single Porudzbina model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Porudzbina model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Danasnji dan
        $dann = date('w');
        // Sutrasnji dan (Porucivanje danas za sutra)
        $dan = $dann + 1;
        $nameOfUser = null;
        $osoba = '';
        $identity = User::findIdentity(Yii::$app->user->id);
        $personId = (int) $identity->id;
        $personName = $identity->username;
       
        if ($identity != null) {
            $nameOfUser =$identity->username;
        }

        // Kriterijum za vadjenje osoba u dropdownu na view index fajlu
        if ($nameOfUser != 'Supervisor' || $nameOfUser == null) {
            $osoba =  Osoba::find()->andWhere('ime ='."'$personName'")->asArray()->all();
        } else {
            $osoba = Osoba::find()->asArray()->all();
        }
        
        $model = new Porudzbina();
        $nedeljaModel = new Nedelja;
      
        // Uzimanje poslednje cene iz baze
        $cena = OdrediCenu::getPrice();
        if ($model->load(Yii::$app->request->post())) {
            $today = date('Y-m-d');
            //Porucivanje rucka danas za sutra (to bi se moglo promeniti na date picker
            // i unaprediti za porucivanje za celu nedelju)
            if (date('D')=='Fri') {
                $model->created_on =date('Y-m-d', strtotime($today . "+3 days"));
            } else {
                $model->created_on =date('Y-m-d', strtotime($today . "+1 days"));
            }
            $model->cena = $cena;
            // setovanje koja osoba je napravila porudzbinu na osnovu login id
            if ($nameOfUser != 'Supervisor') {
                Osoba::getOsobaLikeUser($nameOfUser);
                // var_dump($nameOfUser);die;
               // $model->id_osoba = $personId;
                $model->id_osoba =  Osoba::getOsobaLikeUser($nameOfUser);  // TODO ???????
            }
          
            $model->save();
            return $this->redirect('index');
        }

        $jelo = $nedeljaModel->returnWeek();
        $rez = GlavnoJelo::getMeal($jelo);
        $posnoJelo = 0;

        // Jela za dropdown u formi po nedelji i danu
        $jela = GlavnoJelo::find()->where("nedelja ={$jelo} and dan = {$dan} or nedelja = {$posnoJelo}")
        ->asArray()->all();
       
       
        return $this->render('create', [
            'model' => $model,
            'rez' => $rez,
            'nameOfUser' => $nameOfUser,
            'osoba' => $osoba,
            'jela' => $jela,
        ]);
    }

    /**
     * Updates an existing Porudzbina model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $osoba = "";

        $nameOfUser = null;
        $identity = User::findIdentity(Yii::$app->user->id);
       
        if ($identity != null) {
            $nameOfUser =$identity->username;
        }

        // Kriterijum za vadjenje osoba u dropdownu na view index fajlu
        if ($nameOfUser != 'Supervisor' || $nameOfUser == null) {
            $osoba =  Osoba::find()->andWhere('id_osoba ='."'$personId'")->asArray()->all();
        } else {
            $osoba = Osoba::find()->asArray()->all();
        }

        $model = $this->findModel($id);
        $nedeljaModel = new Nedelja;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id_porudzbina]);
        }
        $jelo = $nedeljaModel->returnWeek();
        $rez = GlavnoJelo::getMeal($jelo);

        // Danasnji dan
        $dann = date('w');
        // Sutrasnji dan (Porucivanje danas za sutra)
        $dan = $dann + 1;

        $posnoJelo = 0;

        // Jela za dropdown u formi po nedelji i danu
        $jela = GlavnoJelo::find()->where("nedelja ={$jelo} and dan = {$dan} or nedelja = {$posnoJelo}")
        ->asArray()->all();
           

        return $this->render('update', [
            'model' => $model,
            'rez' => $rez,
            'jelo' => $jelo,
            'nameOfUser' => $nameOfUser,
            'jela' => $jela,
            'dan' => $dan,
            'osoba' => $osoba,
        ]);
    }

    /**
     * Deletes an existing Porudzbina model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;

        if ($request->isPost) {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }
       
        return $this->redirect(['index']);
    }

    /**
     * Finds the Porudzbina model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Porudzbina the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Porudzbina::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // Slanje email-a i pravljenje excel exporta
    public function actionSend()
    {
        $data = Porudzbina::sqlDataExcel();
        // check if data is empty (if emty must set other date in query data)
        if(empty($data)) {  
            $notSendMessage = 'Your email is not send becouse you don\'t have orders for that date';
            return $this->redirect(['index','notSendMessage' => $notSendMessage]);
        }

        $spreadsheet = new Spreadsheet();
        Porudzbina::exportExcel($data, $spreadsheet);
    
        $setTo = Yii::$app->params['mailTo'];
        $setSubject = 'Porudzbina - klopa';
        $setBody = 'Postovani,
        Ovo je nasa porudzbina za sutra.
        Hvala, Softmetrix Team';
        $atachFile = \Yii::$app->basePath.'\web\Avangarda.xlsx';
         
        $send = Porudzbina::sendMail($setTo, $setSubject, $setBody, $atachFile);

        if ($send) {
            if (file_exists(\Yii::$app->basePath.'\web\Avangarda.xlsx')) {
                \Yii::$app->getSession()->setFlash('success', 'Uspesno ste poslali mail');
                return Yii::$app->response->xSendFile(\Yii::$app->basePath.'\web\Avangarda.xlsx' . ' ' . date('Y-m-d'));
            }
            \Yii::$app->getSession()->setFlash('success', 'Uspesno ste poslali mail');
            return $this->redirect(['index']);
        } else {
            \Yii::$app->getSession()->setFlash('error', 'Email nije poslat!');
            return $this->redirect(['index']);
        }
    }
    // Brisanje selectovanih porudzbina (checkbox)
    public function actionCheck()
    {
        $values = Yii::$app->request->post();
        if ($values && array_key_exists('selection', $values)) {
            $values = Yii::$app->request->post();
            $data = $values["selection"];
            foreach ($data as $id) {
                $this->findModel($id)->delete();
            }
        }
        return $this->redirect('index');
    }
}
