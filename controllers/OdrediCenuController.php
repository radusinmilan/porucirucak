<?php

namespace app\controllers;

use Yii;
use app\models\OdrediCenu;
use app\models\OdrediCenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OdrediCenuController implements the CRUD actions for OdrediCenu model.
 */
class OdrediCenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','create','update','view'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
        ]
        ];
    }

    /**
     * Lists all OdrediCenu models.
     * @return mixed
     */
    /*
    public function actionIndex()
    {
        $searchModel = new OdrediCenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    */
    /**
     * Displays a single OdrediCenu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    */
    /**
     * Creates a new OdrediCenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OdrediCenu();
        $trenutnaCena = OdrediCenu::getPrice();

        if ($model->load(Yii::$app->request->post())) {
            $modelBroj  = (int)$model->trenutna_cena;
            $model ->updatePrice($modelBroj);
            return $this->redirect(['porudzbina/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'trenutnaCena' => $trenutnaCena,
        ]);
    }

    /**
     * Updates an existing OdrediCenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_cena]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    */
    /**
     * Deletes an existing OdrediCenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OdrediCenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OdrediCenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OdrediCenu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
