<?php

namespace app\controllers;

use Yii;
use app\models\Porudzbina;
use app\models\PorudzbinaSearch;
use app\models\User;
use app\models\Nedelja;
use app\models\Osoba;
use app\models\GlavnoJelo;
use app\models\OdrediCenu;
use app\models\Kompanija;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\klase\FiltersStats;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;

/**
 * PorudzbinaController implements the CRUD actions for Porudzbina model.
 */
class ReportsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionRucakMeseca()
    {
        $model = new FiltersStats();

        if (Yii::$app->getRequest()->isPost) {
            $model->load(Yii::$app->getRequest()->post());
        }

       
        $rucakMeseca = Porudzbina::getLunchOfTheMonth($model);
        $rucakMeseca = json_encode($rucakMeseca);

        $items = ArrayHelper::map(Porudzbina::getOrderDate(), 'created_on', 'created_on');
        $model = new FiltersStats();
    
        return $this->render('rucak-meseca', [
            'model' => $model,
            'chartData' => $rucakMeseca,
            'items' => $items,
        ]);
    }

    public function actionDebts()
    {
        $query = Porudzbina::getDebtReport();
        $message = "Izvestaj dugovanja: " . date('Y-m-d');

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->sql,
            'totalCount' => $query->count(),
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);
   
        return $this->render('debts', [
           // 'model' => $model,
            'message'=>$message,
            'dataProvider' => $dataProvider,
        ]);
    }
}
