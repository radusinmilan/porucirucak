<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m191201_095231_menu
 */
class m191201_095231_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER,
            'href' => Schema::TYPE_STRING,
            'icon' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_STRING,
            'order' => Schema::TYPE_INTEGER,
            'description' => Schema::TYPE_TEXT
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191201_095231_menu cannot be reverted.\n";

        return false;
    }
    */
}
